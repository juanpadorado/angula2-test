import { Component } from 'angular2/core';
import { Pelicula } from "../model/pelicula";
import { PeliculasService } from "../services/pelicula.service";

@Component({
    selector: "peliculas-list",
    templateUrl: 'app/view/peliculas-list.html',
    providers: [PeliculasService]
})

export class PeliculasListComponent {

    public pelicula: Pelicula;
    public peliculaElegida: Pelicula;
    public mostrarDatos: boolean;
    public peliculas: Array<Pelicula>;
    public datoServicio;

    constructor(private _peliculasServices: PeliculasService) {
        this.mostrarDatos = false;

        this.peliculas = this._peliculasServices.getPeliculas();

        this.peliculaElegida = this.peliculas[0];
        this.pelicula = this.peliculas[0];
        this.onLog();
    }

    showHide(value) {
        this.mostrarDatos = value;
    }

    onLog(titulo = null) {
        if (titulo != null) {
            console.log(titulo)
        } else {
            console.log(this.pelicula.titulo)
        }

    }

    onCambiarPelicula(pelicula){
        this.pelicula = pelicula;
        this.peliculaElegida = pelicula;
    }
}