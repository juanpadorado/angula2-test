import { Component } from "angular2/core";
import { Pelicula } from "../model/pelicula";
import { Router } from "angular2/router";
import { PeliculasService } from "../services/pelicula.service";

@Component({
    templateUrl: "app/view/crear-pelicula.html",
    providers: [PeliculasService]
})

export class CrearPeliculaComponent {

    constructor(private _peliculaService: PeliculasService, private _router: Router){

    }
    
    onCrearPelicula(titulo, director, anio){
        let pelicula: Pelicula = new Pelicula(77, titulo, director, anio);
        
        this._peliculaService.insertPelicula(pelicula);
        this._router.navigate(["Peliculas"]);
    }

}