import { Component } from "angular2/core";
import { PeliculasListComponent } from "./components/peliculas-list.component";
import { PeliculasFooterComponent } from "./components/peliculas-footer.component";
import { ContactoComponent } from "./components/contacto.component";
import { ROUTER_DIRECTIVES, RouteConfig, Router } from "angular2/router";
import { CrearPeliculaComponent } from "./components/crear-pelicula.component";

@Component({
    selector: "my-app",
    templateUrl: "app/view/peliculas.html",
    directives: [PeliculasListComponent,
        PeliculasFooterComponent,
        ContactoComponent,
        CrearPeliculaComponent,
        ROUTER_DIRECTIVES]
})

@RouteConfig([
    { path: "/peliculas", name: "Peliculas", component: PeliculasListComponent, useAsDefault: true },
    { path: "/contacto", name: "Contacto", component: ContactoComponent },
    { path: "/crear-pelicula", name: "CrearPelicula", component: CrearPeliculaComponent }
])

export class AppComponent {
    public titulo: string = "Peliculas con angular2";

}