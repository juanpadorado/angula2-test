System.register(["../model/pelicula"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var pelicula_1;
    var PELICULAS;
    return {
        setters:[
            function (pelicula_1_1) {
                pelicula_1 = pelicula_1_1;
            }],
        execute: function() {
            exports_1("PELICULAS", PELICULAS = [
                new pelicula_1.Pelicula(1, 'Batman VS Superman', 'Juan ca', 2016),
                new pelicula_1.Pelicula(2, 'La verdad duele', 'Arnol', 2017),
                new pelicula_1.Pelicula(3, 'Spiderman', 'chucknorris', 2012),
                new pelicula_1.Pelicula(4, 'Hombres x', 'don ramon', 2015),
                new pelicula_1.Pelicula(5, 'Don jon', 'Flavio', 2016),
                new pelicula_1.Pelicula(6, 'El chavo', 'Don rambo', 2013)
            ]);
        }
    }
});
//# sourceMappingURL=mock.peliculas.js.map