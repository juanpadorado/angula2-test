import { Pelicula } from "../model/pelicula";

export const PELICULAS: Pelicula[] = [
    new Pelicula(1, 'Batman VS Superman', 'Juan ca', 2016),
    new Pelicula(2, 'La verdad duele', 'Arnol', 2017),
    new Pelicula(3, 'Spiderman', 'chucknorris', 2012),
    new Pelicula(4, 'Hombres x', 'don ramon', 2015),
    new Pelicula(5, 'Don jon', 'Flavio', 2016),
    new Pelicula(6, 'El chavo', 'Don rambo', 2013)
];